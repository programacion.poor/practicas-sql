# Solucionario de Valores por defecto (default)
## Ejercicio propuesto
### Ejercicio 01

# 1 Elimine la tabla "visitantes" solucion:

```sql
drop table visitantes;

```

# 2 Cree la tabla con la siguiente estructurad solucion:

```sql
create table visitantes(
nombre varchar2(30),
edad number(2),
sexo char(1) default 'f',
domicilio varchar2(30),
ciudad varchar2(20) default 'Cordoba',
telefono varchar(11),
mail varchar(30) default 'no tiene',
montocompra number (6,2)
);
```

# 3 Analice la información que retorna la siguiente consulta solucion:
```sql

select column_name, nullable, data_default
from user_tab_columns where TABLE_NAME = 'VISITANTES';

```
# 4 Ingrese algunos registros sin especificar valores para algunos campos para ver cómo opera la cláusula "default" solucion:
```sql

insert into visitantes (domicilio, ciudad, telefono, mail, montocompra)
values ('Colon 123', 'Cordoba', '4334455', 'juanlopez@hotmail.com', 59.80);

insert into visitantes (nombre, edad, sexo, telefono, mail, montocompra)
values ('Marcos Torres', 29, 'm', '4112233', 'marcostorres@hotmail.com', 60);

insert into visitantes (nombre, edad, sexo, domicilio, ciudad)
values ('Susana Molina', 43, 'f', 'Bulnes 345', 'Carlos Paz');
```

# 5 Recupere todos los registros solucion:

```sql
select * from visitantes;

```


# 6 Use la palabra "default" para ingresar valores en un "insert" solucion:

```sql
insert into visitantes (nombre, edad, sexo, domicilio, ciudad, telefono, mail, montocompra)
values ('Laura Gomez', 35, default, 'Av. San Martin 567', default, '5556677', default, 75.50);
```
# 7 Recupere el registro anteriormente ingresado solucion:

```sql
select * from visitantes where nombre = 'Laura Gomez';
```
# Ejercicio 02

# 1 Elimine la tabla "prestamos" solucion:

```sql
drop table prestamos;
```
# 2 Cree la tabla:
```sql
create table prestamos(
    titulo varchar2(40) not null,
    documento char(8) not null,
    fechaprestamo date not null,
    fechadevolucion date,
    devuelto char(1) default 'n'
);
```
# 3 Consulte "user_tab_columns" y analice la información solucion:
```sql
select column_name, nullable, data_default
from user_tab_columns where TABLE_NAME = 'PRESTAMOS';
```
# 4 Ingrese algunos registros omitiendo el valor para los campos que lo admiten solucion:
```sql
insert into prestamos (titulo, documento, fechaprestamo)
values ('Cien años de soledad', '12345678', to_date('2023-06-01', 'YYYY-MM-DD'));

insert into prestamos (titulo, documento, fechaprestamo)
values ('Don Quijote de la Mancha', '87654321', to_date('2023-05-30', 'YYYY-MM-DD'));
```
# 5 Seleccione todos los registros solucion:
```sql
select * from prestamos;
```
# 6 Ingrese un registro colocando "default" en los campos que lo admiten y vea cómo se almacenó solucion:
```sql
insert into prestamos (titulo, documento, fechaprestamo, fechadevolucion, devuelto)
values ('1984', '56789012', default, default, default);

select * from prestamos where titulo = '1984';
```
# 7 Intente ingresar un registro colocando "default" como valor para un campo que no admita valores nulos y no tenga definido un valor por defecto solucion:
```sql
insert into prestamos (titulo, documento, fechaprestamo, fechadevolucion, devuelto)
values ('La sombra del viento', '34567890', default, default, 'n');
```








