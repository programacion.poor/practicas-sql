# Solucionario de Restricciones (foreign key)

# Ejercicios propuestos

# Ejecicio01

# 1. Eliminar las tablas solucion
```sql
DROP TABLE clientes;
DROP TABLE provincias;
```
# 2. Crear las tablas
```sql
CREATE TABLE clientes (
    codigo NUMBER(5),
    nombre VARCHAR2(30),
    domicilio VARCHAR2(30),
    ciudad VARCHAR2(20),
    codigoprovincia NUMBER(2),
    CONSTRAINT fk_codigoprovincia FOREIGN KEY (codigoprovincia) REFERENCES provincias (codigo)
);

CREATE TABLE provincias (
    codigo NUMBER(2),
    nombre VARCHAR2(20),
    CONSTRAINT pk_codigo PRIMARY KEY (codigo)
);
```
# 3. Establecer una restricción "unique" al campo "codigo" de "provincias" solucion
```sql
ALTER TABLE provincias ADD CONSTRAINT uk_codigo UNIQUE (codigo);
```
# 4. Insertar algunos registros para ambas tablas solucion
```sql
INSERT INTO provincias VALUES (1, 'Cordoba');
INSERT INTO provincias VALUES (2, 'Santa Fe');
INSERT INTO provincias VALUES (3, 'Misiones');
INSERT INTO provincias VALUES (4, 'Rio Negro');

INSERT INTO clientes VALUES (100, 'Perez Juan', 'San Martin 123', 'Carlos Paz', 1);
INSERT INTO clientes VALUES (101, 'Moreno Marcos', 'Colon 234', 'Rosario', 2);
INSERT INTO clientes VALUES (102, 'Acosta Ana', 'Avellaneda 333', 'Posadas', 3);
INSERT INTO clientes VALUES (103, 'Luisa Lopez', 'Juarez 555', 'La Plata', 6);
```
# 5. Intentar agregar la restricción "foreign key" del punto 2 a la tabla "clientes" solucion
```sql
ALTER TABLE clientes ADD CONSTRAINT fk_codigoprovincia FOREIGN KEY (codigoprovincia) REFERENCES provincias (codigo);
-- ERROR: ORA-02291: integrity constraint (FK_CODIGOPROVINCIA) violated - parent key not found

```
# 6. Eliminar el registro de "clientes" que no cumple con la restricción y establecer la restricción nuevamente solucion
```sql
DELETE FROM clientes WHERE codigoprovincia = 6;
ALTER TABLE clientes ADD CONSTRAINT fk_codigoprovincia FOREIGN KEY (codigoprovincia) REFERENCES provincias (codigo);
```
# 7. Intentar agregar un cliente con un código de provincia inexistente en "provincias" solucion
```sql
INSERT INTO clientes VALUES (104, 'Gonzalez Maria', 'Belgrano 456', 'Mar del Plata', 5);
-- ERROR: ORA-02291: integrity constraint (FK_CODIGOPROVINCIA) violated - parent key not found
```
# 8. Intentar eliminar el registro con código 3 de "provincias" solucion
```sql
DELETE FROM provincias WHERE codigo = 3;
-- ERROR: ORA-02292: integrity constraint (FK_CODIGOPROVINCIA) violated - child record found
```
# 9. Eliminar el registro con código 4 de "provincias" solucion
```sql
DELETE FROM provincias WHERE codigo = 4;
```
# 10. Intentar modificar el registro con código 1 de "provincias" solucion
```sql
UPDATE provincias SET codigo = 5 WHERE codigo = 1;
-- ERROR: ORA-02292: integrity constraint (FK_CODIGOPROVINCIA) violated - child record found
```
# 11. Ver las restricciones de "clientes" consultando "user_constraints" solucion
```sql 
SELECT constraint_name, constraint_type
FROM user_constraints
WHERE table_name = 'CLIENTES';
```
# 12. Ver las restricciones de "provincias" solucion
```sql
SELECT constraint_name, constraint_type
FROM user_constraints
WHERE table_name = 'PROVINCIAS';
```
# 13. Intentar eliminar la tabla "provincias" (mensaje de error) solucion
```sql
DROP TABLE provincias;
-- ERROR: ORA-02449: unique/primary keys in table referenced by foreign keys
```
# 14. Eliminar la restricción "foreign key" de "clientes" y luego eliminar la tabla "provincias" solucion
```sql
ALTER TABLE clientes DROP CONSTRAINT fk_codigoprovincia;
DROP TABLE provincias;
```