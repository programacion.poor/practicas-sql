# Solucionario de Disparador de actualización - lista de campos (update trigger)

# Ejercicios popuestos

# 1. Elimine las tablas:
```sql
DROP TABLE control;
DROP TABLE libros;
```


# 2. Cree las tablas con las siguientes estructuras:
```sql
CREATE TABLE libros(
    codigo NUMBER(6),
    titulo VARCHAR2(40),
    autor VARCHAR2(30),
    editorial VARCHAR2(20),
    precio NUMBER(6,2)
);

CREATE TABLE control(
    usuario VARCHAR2(30),
    fecha DATE
);
```


# 3. Ingrese algunos registros en "libros":
```sql
INSERT INTO libros VALUES(100,'Uno','Richard Bach','Planeta',25);
INSERT INTO libros VALUES(103,'El aleph','Borges','Emece',28);
INSERT INTO libros VALUES(105,'Matematica estas ahi','Paenza','Nuevo siglo',12);
INSERT INTO libros VALUES(120,'Aprenda PHP','Molina Mario','Nuevo siglo',55);
INSERT INTO libros VALUES(145,'Alicia en el pais de las maravillas','Carroll','Planeta',35);
```


# 4. Establezca el formato de fecha para que muestre "DD/MM/YYYY HH24:MI":
```sql
ALTER SESSION SET NLS_DATE_FORMAT = 'DD/MM/YYYY HH24:MI';
```


# 5. Cree un desencadenador a nivel de sentencia que se dispare cada vez que se actualicen los campos "precio" y "editorial"; el trigger debe ingresar en la tabla "control", el nombre del usuario, la fecha y la hora en la cual se realizó un "update" sobre "precio" o "editorial" de "libros"
```sql
CREATE OR REPLACE TRIGGER trg_actualizacion_libros
AFTER UPDATE OF precio, editorial ON libros
FOR EACH ROW
BEGIN
    INSERT INTO control(usuario, fecha)
    VALUES(USER, SYSDATE);
END;
/
```


# 6. Vea qué informa el diccionario "user_triggers" respecto del trigger anteriormente creado
```sql
SELECT * FROM user_triggers WHERE trigger_name = 'TRG_ACTUALIZACION_LIBROS';
```


# 7. Aumente en un 10% el precio de todos los libros de editorial "Nuevo siglo"
```sql
UPDATE libros SET precio = precio * 1.1 WHERE editorial = 'Nuevo siglo';
```


# 8. Vea cuántas veces se disparó el trigger consultando la tabla "control"
```sql
SELECT COUNT(*) FROM control;
```


# 9. Cambie la editorial, de "Planeta" a "Sudamericana"
```sql
UPDATE libros SET editorial = 'Sudamericana' WHERE editorial = 'Planeta';
```


# 10. Veamos si el trigger se disparó consultando la tabla "control"
```sql
SELECT COUNT(*) FROM control;
```


# 11. Modifique un campo diferente de los que activan el trigger
```sql
UPDATE libros SET titulo = 'Nueva edicion' WHERE codigo = 100;
```


# 12. Verifique que el cambio se realizó
```sql
SELECT * FROM libros WHERE codigo = 100;
```


# 13. Verifique que el trigger no se disparó
```sql
SELECT COUNT(*) FROM control;
```

