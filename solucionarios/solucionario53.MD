# Solucionario de Combinaciones y funciones de agrupamiento

# Ejercicios propuestos

# 1. Eliminar las tablas
```sql
DROP TABLE visitantes;
DROP TABLE ciudades;
```
# 2. Crear las tablas
```sql
CREATE TABLE visitantes (
    nombre VARCHAR2(30),
    edad NUMBER(2),
    sexo CHAR(1) DEFAULT 'f',
    domicilio VARCHAR2(30),
    codigociudad NUMBER(2),
    mail VARCHAR(30),
    montocompra DECIMAL(6, 2)
);

CREATE TABLE ciudades (
    codigo NUMBER(2),
    nombre VARCHAR(20)
);
```
# 3. Insertar algunos registros
```sql
INSERT INTO ciudades VALUES (1, 'Cordoba');
INSERT INTO ciudades VALUES (2, 'Carlos Paz');
INSERT INTO ciudades VALUES (3, 'La Falda');
INSERT INTO ciudades VALUES (4, 'Cruz del Eje');

INSERT INTO visitantes VALUES ('Susana Molina', 35, 'f', 'Colon 123', 1, NULL, 59.80);
INSERT INTO visitantes VALUES ('Marcos Torres', 29, 'm', 'Sucre 56', 1, 'marcostorres@hotmail.com', 150.50);
INSERT INTO visitantes VALUES ('Mariana Juarez', 45, 'f', 'San Martin 111', 2, NULL, 23.90);
INSERT INTO visitantes VALUES ('Fabian Perez', 36, 'm', 'Avellaneda 213', 3, 'fabianperez@xaxamail.com', 0);
INSERT INTO visitantes VALUES ('Alejandra Garcia', 28, 'f', NULL, 2, NULL, 280.50);
INSERT INTO visitantes VALUES ('Gaston Perez', 29, 'm', NULL, 5, 'gastonperez1@gmail.com', 95.40);
INSERT INTO visitantes VALUES ('Mariana Juarez', 33, 'f', NULL, 2, NULL, 90);
```
# 4. Contar la cantidad de visitas por ciudad mostrando el nombre de la ciudad (3 filas)
```sql
SELECT c.nombre AS ciudad, COUNT(*) AS cantidad_visitas
FROM ciudades c
JOIN visitantes v ON c.codigo = v.codigociudad
GROUP BY c.nombre;
```
# 5. Mostrar el promedio de gastos de las visitas agrupados por ciudad y sexo (4 filas)
```sql
SELECT c.nombre AS ciudad, v.sexo, AVG(v.montocompra) AS promedio_gastos
FROM ciudades c
JOIN visitantes v ON c.codigo = v.codigociudad
GROUP BY c.nombre, v.sexo;
```
# 6. Mostrar la cantidad de visitantes con mail, agrupados por ciudad (3 filas)
```sql
SELECT c.nombre AS ciudad, COUNT(v.mail) AS cantidad_visitantes_con_mail
FROM ciudades c
LEFT JOIN visitantes v ON c.codigo = v.codigociudad
GROUP BY c.nombre;
```
# 7. Obtener el monto de compra más alto de cada ciudad (3 filas)
```sql
SELECT c.nombre AS ciudad, MAX(v.montocompra) AS monto_compra_mas_alto
FROM ciudades c
JOIN visitantes v ON c.codigo = v.codigociudad
GROUP BY c.nombre;
```
# 8. Realizar la misma consulta anterior pero con "left join" (4 filas)
```sql
SELECT c.nombre AS ciudad, MAX(v.montocompra) AS monto_compra_mas_alto
FROM ciudades c
LEFT JOIN visitantes v ON c.codigo = v.codigociudad
GROUP BY c.nombre;
```





