# Solcionario de Alterar secuencia (alter sequence)

# Ejecicio propesto


# 1. Eliminar la tabla "empleados" solucion
```sql
drop table empleados;
```
# 2. Crear la tabla con los campos especificados y una clave primaria solucion
```sql
create table empleados(
    legajo number(3),
    documento char(8) not null,
    nombre varchar2(30) not null,
    primary key(legajo)
);
```
# 3. Eliminar la secuencia "sec_legajoempleados" si existe solucion
```sql
begin
    execute immediate 'drop sequence sec_legajoempleados';
    exception when others then null;
end;

```
# Crear la secuencia con los parámetros especificados solucion
```sql
create sequence sec_legajoempleados
    minvalue 1
    maxvalue 210
    start with 206
    increment by 2
    nocycle;
```
# Inicializar la secuencia solucion
```sql
select sec_legajoempleados.nextval from dual;
```
# 4. Insertar registros utilizando la secuencia para los valores de la clave primaria solucion
```sql
insert into empleados
values (sec_legajoempleados.currval, '22333444', 'Ana Acosta');

insert into empleados
values (sec_legajoempleados.nextval, '23444555', 'Betina Bustamante');

insert into empleados
values (sec_legajoempleados.nextval, '24555666', 'Carlos Caseros');
```
# 5. Recuperar los registros de "empleados" para ver los valores de la clave primaria solucion
```sql
select * from empleados;
```
# 6. Ver el valor actual de la secuencia utilizando la tabla "dual" solucion
```sql
select sec_legajoempleados.currval from dual;
```
# 7. Intentar ingresar un registro empleando "nextval" solucion
```sql
-- Esto dará un error ya que la secuencia ha alcanzado su valor máximo
insert into empleados
values (sec_legajoempleados.nextval, '25666777', 'Diana Dominguez');
```
# 8. Alterar la secuencia modificando el atributo "maxvalue" a 999 solucion
```sql
alter sequence sec_legajoempleados maxvalue 999;
```
# 9. Obtener información de la secuencia solucion
```sql
select sequence_name, min_value, max_value, increment_by from user_sequences where sequence_name = 'SEC_LEGAJOEMPLEADOS';
```
# 10. Ingresar el registro del punto 7 solucion
```sql
insert into empleados
values (sec_legajoempleados.nextval, '25666777', 'Diana Dominguez');
```
# 11. Recuperar los registros de "empleados" solucion
```sql
select * from empleados;
```
# 12. Modificar la secuencia para que sus valores se incrementen en 1 solucion
```sql
alter sequence sec_legajoempleados increment by 1;
```
# 13. Ingresar un nuevo registro solucion
```sql
insert into empleados
values (sec_legajoempleados.nextval, '26777888', 'Federico Fuentes');
```
# 14. Recuperar los registros de "empleados" solucion
```sql
select * from empleados;
```
# 15. Eliminar la secuencia creada solucion
```sql
drop sequence sec_legajoempleados;
```
# 16. Consultar todos los objetos de la base de datos que sean secuencias y verificar que "sec_legajoempleados" ya no existe solucion
```sql
select object_name, object_type from user_objects where object_type = 'SEQUENCE';
```






