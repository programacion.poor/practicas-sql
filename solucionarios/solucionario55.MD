# Solucionario de Otros tipos de combinaciones

# Ejercicios propuestos

# 1. Eliminar las tablas solucion 
```sql
DROP TABLE clientes;
DROP TABLE provincias;
```
# 2. Crear las tablas solucion
```sql
CREATE TABLE clientes (
    codigo NUMBER(5),
    nombre VARCHAR2(30),
    domicilio VARCHAR2(30),
    ciudad VARCHAR2(20),
    codigoprovincia NUMBER(2)
);

CREATE TABLE provincias (
    codigoprovincia NUMBER(2),
    nombre VARCHAR2(20)
);
```
# 3. Insertar algunos registros para ambas tablas solucion
```sql
INSERT INTO provincias VALUES (1, 'Cordoba');
INSERT INTO provincias VALUES (2, 'Santa Fe');
INSERT INTO provincias VALUES (3, 'Corrientes');
INSERT INTO provincias VALUES (NULL, 'Salta');

INSERT INTO clientes VALUES (100, 'Lopez Marcos', 'Colon 111', 'Córdoba', 1);
INSERT INTO clientes VALUES (101, 'Perez Ana', 'San Martin 222', 'Cruz del Eje', 1);
INSERT INTO clientes VALUES (102, 'Garcia Juan', 'Rivadavia 333', 'Villa Maria', 1);
INSERT INTO clientes VALUES (103, 'Perez Luis', 'Sarmiento 444', 'Rosario', 2);
INSERT INTO clientes VALUES (104, 'Gomez Ines', 'San Martin 666', 'Santa Fe', 2);
INSERT INTO clientes VALUES (105, 'Torres Fabiola', 'Alem 777', 'La Plata', 4);
INSERT INTO clientes VALUES (106, 'Garcia Luis', 'Sucre 475', 'Santa Rosa', NULL);
```
# 3. Mostrar todos los datos de los clientes, incluido el nombre de la provincia empleando un "left join" (7 filas) solucion
```sql
SELECT c.*, p.nombre AS provincia
FROM clientes c
LEFT JOIN provincias p ON c.codigoprovincia = p.codigoprovincia;
```
# 4. Obtenga la misma salida que la consulta anterior pero empleando un "join" con el modificador (+) solucion
```sql
SELECT c.*, p.nombre AS provincia
FROM clientes c, provincias p
WHERE c.codigoprovincia = p.codigoprovincia(+);
```
# 5. Mostrar todos los datos de los clientes, incluido el nombre de la provincia empleando un "right join" para que las provincias de las cuales no hay clientes también aparezcan en la consulta (7 filas) solucion
```sql
SELECT c.*, p.nombre AS provincia
FROM clientes c
RIGHT JOIN provincias p ON c.codigoprovincia = p.codigoprovincia;
```
# 6. Obtenga la misma salida que la consulta anterior pero empleando un "join" con el modificador (+) solucion
```sql
SELECT c.*, p.nombre AS provincia
FROM clientes c, provincias p
WHERE c.codigoprovincia(+) = p.codigoprovincia;
```
# 7. Intente colocar en una consulta "join", el modificador "(+)" en ambos campos del enlace (mensaje de error) solucion
```sql
SELECT c.*, p.nombre AS provincia
FROM clientes c, provincias p
WHERE c.codigoprovincia(+) = p.codigoprovincia(+);
-- Esto genera un mensaje de error ya que no se permite utilizar el modificador (+) en ambos campos del enlace.
```
# 8. Intente realizar un natural join entre ambas tablas mostrando el nombre del cliente, la ciudad y nombre de la provincia (las tablas tienen 2 campos con  igual nombre "codigoprovincia" y "nombre"; mensaje de error) solucion

```sql
SELECT nombre, ciudad, nombre
FROM clientes NATURAL JOIN provincias;
-- Esto genera un mensaje de error ya que ambas tablas tienen columnas con nombres duplicados.
```
# 9. Realice una combinación entre ambas tablas empleando la cláusula "using" (5 filas) solucion
```sql
SELECT c.nombre, c.ciudad, p.nombre AS provincia
FROM clientes c
JOIN provincias p USING (codigoprovincia);
```