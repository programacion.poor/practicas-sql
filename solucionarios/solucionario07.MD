# solucionario Borrar registros (delete)
## ejercicio de laboratorio

```sql
-- 1. Eliminamos la tabla "usuarios" si existe
DROP TABLE usuarios;

-- Salida: La tabla "usuarios" ha sido eliminada exitosamente.
```
```sql
-- 2. Creamos una nueva tabla llamada "usuarios" con los campos especificados
CREATE TABLE usuarios(
    nombre varchar2(30),
    clave varchar2(10)
);

-- Salida: La tabla "usuarios" ha sido creada exitosamente.
```
```sql
-- 3. Agregamos registros a la tabla
INSERT INTO usuarios (nombre, clave) VALUES ('Marcelo', 'River');
INSERT INTO usuarios (nombre, clave) VALUES ('Susana', 'chapita');
INSERT INTO usuarios (nombre, clave) VALUES ('CarlosFuentes', 'Boca');
INSERT INTO usuarios (nombre, clave) VALUES ('FedericoLopez', 'Boca');

-- Salida: 4 filas han sido insertadas correctamente.
```
```sql
-- 4. Seleccionamos todos los registros de la tabla
SELECT * FROM usuarios;

-- Salida:
+----------------+--------+
|     NOMBRE     | CLAVE  |
+----------------+--------+
|    Marcelo     | River  |
|    Susana      | chapita|
| CarlosFuentes  | Boca   |
| FedericoLopez  | Boca   |
+----------------+--------+
```
```sql
-- 5. Vamos a eliminar el registro cuyo nombre de usuario es "Marcelo"
DELETE FROM usuarios WHERE nombre = 'Marcelo';

-- Salida: Se ha borrado 1 fila.
```
```sql
-- 6. Intentamos eliminarlo nuevamente
DELETE FROM usuarios WHERE nombre = 'Marcelo';

-- Salida: No se ha borrado ninguna fila.
```
```sql
-- 7. Eliminamos todos los registros cuya clave es 'Boca'
DELETE FROM usuarios WHERE clave = 'Boca';

-- Salida: Se han eliminado 2 registros.
```
```sql
-- 8. Eliminemos todos los registros
DELETE FROM usuarios;

-- Salida: Todos los registros han sido eliminados.
```
```sql
-- 9. Veamos el contenido de la tabla
SELECT * FROM usuarios;

-- Salida: No hay registros.
```
```sql
-- 10. Ingresemos el siguiente lote de comandos en el Oracle SQL Developer
DROP TABLE usuarios;

CREATE TABLE usuarios(
    nombre varchar2(30),
    clave varchar2(10)
);

INSERT INTO usuarios (nombre, clave) VALUES ('Marcelo', 'River');
INSERT INTO usuarios (nombre, clave) VALUES ('Susana', 'chapita');
INSERT INTO usuarios (nombre, clave) VALUES ('CarlosFuentes', 'Boca');
INSERT INTO usuarios (nombre, clave) VALUES ('FedericoLopez', 'Boca');

SELECT * FROM usuarios;

-- Salida:
+----------------+--------+
|     NOMBRE     | CLAVE  |
+----------------+--------+
|    Marcelo     | River  |
|    Susana      | chapita|
| CarlosFuentes  | Boca   |
| FedericoLopez  | Boca   |
+----------------+--------+

DELETE FROM usuarios WHERE nombre = 'Marcelo';

-- Salida: Se ha borrado 1 fila.

DELETE FROM usuarios WHERE nombre = 'Marcelo';

-- Salida: No se ha borrado ninguna fila.

DELETE FROM usuarios WHERE clave = 'Boca';

-- Salida: Se han eliminado 2 registros.

DELETE FROM usuarios;

-- Salida: Todos los registros han sido eliminados.

SELECT * FROM usuarios;

-- Salida: No hay registros.
```
# ejercicios propuestos
## Ejercicio 01
```sql
-- 1. Elimine la tabla "agenda" si existe
DROP TABLE agenda;

-- Salida: La tabla "agenda" ha sido eliminada exitosamente.
```
```sql
-- 2. Cree la tabla con los campos especificados
CREATE TABLE agenda(
    apellido varchar2(30),
    nombre varchar2(20),
    domicilio varchar2(30),
    telefono varchar2(11)
);

-- Salida: La tabla "agenda" ha sido creada exitosamente.
```
```sql
-- 3. Ingrese los registros a la tabla
INSERT INTO agenda(apellido, nombre, domicilio, telefono) VALUES ('Alvarez', 'Alberto', 'Colon 123', '4234567');
INSERT INTO agenda(apellido, nombre, domicilio, telefono) VALUES ('Juarez', 'Juan', 'Avellaneda 135', '4458787');
INSERT INTO agenda(apellido, nombre, domicilio, telefono) VALUES ('Lopez', 'Maria', 'Urquiza 333', '4545454');
INSERT INTO agenda(apellido, nombre, domicilio, telefono) VALUES ('Lopez', 'Jose', 'Urquiza 333', '4545454');
INSERT INTO agenda(apellido, nombre, domicilio, telefono) VALUES ('Salas', 'Susana', 'Gral. Paz 1234', '4123456');

-- Salida: 5 filas han sido insertadas correctamente.
```
```sql
-- 4. Elimine el registro cuyo nombre sea "Juan"
DELETE FROM agenda WHERE nombre = 'Juan';

-- Salida: Se ha borrado 1 fila.
```
```sql
-- 5. Elimine los registros cuyo número telefónico sea igual a "4545454"
DELETE FROM agenda WHERE telefono = '4545454';

-- Salida: Se han eliminado 2 registros.
```
```sql
-- 6. Elimine todos los registros
DELETE FROM agenda;

-- Salida: Todos los registros han sido eliminados.
```
# Ejercicio 02

```sql
-- 1. Elimine la tabla "articulos" si existe
DROP TABLE articulos;

-- Salida: La tabla "articulos" ha sido eliminada exitosamente (si existía).
```
```sql
-- 2. Cree la tabla con la estructura especificada
CREATE TABLE articulos(
    codigo number(4,0),
    nombre varchar2(20),
    descripcion varchar2(30),
    precio number(7,2),
    cantidad number(3)
);

-- Salida: La tabla "articulos" ha sido creada exitosamente.
```
```sql
-- 3. Vea la estructura de la tabla
DESCRIBE articulos;

-- Salida:
+-------------+--------------+
|   COLUMN    |     TYPE     |
+-------------+--------------+
|   CODIGO    | NUMBER(4,0)  |
|   NOMBRE    | VARCHAR2(20) |
| DESCRIPCION | VARCHAR2(30) |
|   PRECIO    | NUMBER(7,2)  |
|  CANTIDAD   | NUMBER(3)    |
+-------------+--------------+
```
```sql
-- 4. Ingrese algunos registros a la tabla
INSERT INTO articulos (codigo, nombre, descripcion, precio, cantidad) VALUES (1, 'impresora', 'Epson Stylus C45', 400.80, 20);
INSERT INTO articulos (codigo, nombre, descripcion, precio, cantidad) VALUES (2, 'impresora', 'Epson Stylus C85', 500, 30);
INSERT INTO articulos (codigo, nombre, descripcion, precio, cantidad) VALUES (3, 'monitor', 'Samsung 14', 800, 10);
INSERT INTO articulos (codigo, nombre, descripcion, precio, cantidad) VALUES (4, 'teclado', 'ingles Biswal', 100, 50);
INSERT INTO articulos (codigo, nombre, descripcion, precio, cantidad) VALUES (5, 'teclado', 'español Biswal', 90, 50);

-- Salida: 5 filas han sido insertadas correctamente.
```
```sql
-- 5. Elimine los artículos cuyo precio sea mayor o igual a 500
DELETE FROM articulos WHERE precio >= 500;

-- Salida: Se han eliminado 2 registros.
```
```sql
-- 6. Elimine todas las impresoras
DELETE FROM articulos WHERE nombre = 'impresora';

-- Salida: Se ha eliminado 1 registro.
```
```sql
-- 7. Elimine todos los artículos cuyo código sea diferente a 4
DELETE FROM articulos WHERE codigo <> 4;

-- Salida: Se ha eliminado 1 registro.
```
