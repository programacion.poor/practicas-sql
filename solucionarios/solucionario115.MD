# Solucionario de Disparador (eliminar)

# Ejercicios propuestos

# 1. Elimine las tablas:
```sql
drop table ventas;
drop table articulos;
```


# 2. Cree las tablas con las siguientes estructuras:
```sql
create table articulos(
    codigo number(4) not null,
    descripcion varchar2(40),
    precio number (6,2),
    stock number(4),
    constraint PK_articulos_codigo
    primary key (codigo)
);

create table ventas(
    codigo number(4),
    cantidad number(4),
    fecha date,
    constraint FK_ventas_articulos
    foreign key (codigo)
    references articulos(codigo)
);
```


# 3. Cree una secuencia llamada "sec_codigoart", estableciendo que comience en 1, sus valores estén entre 1 y 9999 y se incrementen en 1. Antes elimínela por si existe.
```sql
drop sequence sec_codigoart;
create sequence sec_codigoart
    start with 1
    increment by 1
    minvalue 1
    maxvalue 9999
    nocache;
```


# 4. Active el paquete para permitir mostrar salida en pantalla.
```sql
set serveroutput on;
```


# 5. Cree un trigger que coloque el siguiente valor de una secuencia para el código de "articulos" cada vez que se ingrese un nuevo artículo.
# Podemos ingresar un nuevo registro en "articulos" sin incluir el código porque lo ingresará el disparador luego de calcularlo.
# Si al ingresar un registro en "articulos" incluimos un valor para código, será ignorado y reemplazado por el valor calculado por el disparador.
```sql
create or replace trigger tr_articulos_codigo
    before insert on articulos
    for each row
begin
    :new.codigo := sec_codigoart.nextval;
end;
/
```


# 6. Ingrese algunos registros en "articulos" sin incluir el código:
```sql
insert into articulos (descripcion, precio, stock) values ('cuaderno rayado 24h',4.5,100);
insert into articulos (descripcion, precio, stock) values ('cuaderno liso 12h',3.5,150);
insert into articulos (descripcion, precio, stock) values ('lapices color x6',8.4,60);
```


# 7. Recupere todos los artículos para ver cómo se almacenó el código
```sql
select * from articulos;
```


# 8. Ingrese algunos registros en "articulos" incluyendo el código:
```sql
insert into articulos values(160,'regla 20cm.',6.5,40);
insert into articulos values(173,'compas metal',14,35);
insert into articulos values(234,'goma lapiz',0.95,200);
```


# 9. Recupere todos los artículos para ver cómo se almacenó los códigos
# Ignora los códigos especificados ingresando el siguiente de la secuencia.
```sql
select * from articulos;
```


# 10. Cuando se ingresa un registro en "ventas", se debe: Controlar que el código del artículo exista en "articulos" (lo hacemos con la restricción "foreign key" establecida en "ventas"). Controlar que exista stock, lo cual no puede controlarse con una restricción "foreign key" porque el campo "stock" no es clave primaria en la tabla "articulos". Cree un trigger. Si existe stock, debe disminuirse en "articulos".

# Cree un trigger a nivel de fila sobre la tabla "ventas" para el evento se inserción. Cada vez que se realiza un "insert" sobre "ventas", el disparador se ejecuta.El disparador controla que la cantidad que se intenta vender sea menor o igual al stock del articulo y actualiza el campo "stock" de "articulos",restando al valor anterior la cantidad vendida.Si la cantidad supera el stock, debe producirse un error, revertirse la acción y mostrar un mensaje.
```sql
create or replace trigger tr_insertar_ventas
    before insert on ventas
    for each row
declare
    stock_actual number;
begin
    select stock into stock_actual from articulos where codigo = :new.codigo;
    if :new.cantidad <= stock_actual then
        update articulos set stock = stock - :new.cantidad where codigo = :new.codigo;
        dbms_output.put_line('tr_insertar_ventas activado');
    else
        raise_application_error(-20001, 'No hay suficiente stock para realizar la venta');
    end if;
end;
/
```


# 11. Ingrese un registro en "ventas" cuyo código no exista en "articulos" Aparece un mensaje de error porque el código no existe. El trigger se ejecutó.
```sql
insert into ventas values (999, 2, sysdate);
```


# 12. Verifique que no se ha agregado ningún registro en "ventas"
```sql
select * from ventas;
```


# 13. Ingrese un registro en "ventas" cuyo código exista en "articulos" y del cual haya suficiente stock Note que el trigger se disparó, aparece el texto "tr_insertar_ventas activado".
```sql
insert into ventas values (160, 1, sysdate);
```


# 14. Verifique que el trigger se disparó consultando la tabla "articulos" (debe haberse disminuido el stock) y se agregó un registro en "ventas"
```sql
select * from articulos;
select * from ventas;
```


# 15. Ingrese un registro en "ventas" cuyo código exista en "articulos" y del cual NO haya suficiente stock Aparece el mensaje de error 20001 y el texto que muestra que se disparó el trigger.
```sql
insert into ventas values (160, 10, sysdate);
```


# 16. Verifique que NO se ha disminuido el stock en "articulos" ni se ha agregado un registro en "ventas"
```sql
select * from articulos;
select * from ventas;
```


# 17. El comercio quiere que se realicen las ventas de lunes a viernes de 8 a 18 hs. Reemplace el trigger creado anteriormente "tr_insertar_ventas" para que NO permita que se realicen ventas fuera de los días y horarios especificados y muestre un mensaje de error.
```sql
create or replace trigger tr_insertar_ventas
    before insert on ventas
    for each row
declare
    stock_actual number;
    dia_semana varchar2(20);
    hora_actual varchar2(20);
begin
    select to_char(sysdate, 'Day') into dia_semana from dual;
    select to_char(sysdate, 'HH24:MI:SS') into hora_actual from dual;

    if dia_semana in ('Saturday', 'Sunday') or hora_actual not between '08:00:00' and '18:00:00' then
        raise_application_error(-20001, 'No se pueden realizar ventas fuera del horario permitido');
    end if;

    select stock into stock_actual from articulos where codigo = :new.codigo;
    if :new.cantidad <= stock_actual then
        update articulos set stock = stock - :new.cantidad where codigo = :new.codigo;
        dbms_output.put_line('tr_insertar_ventas activado');
    else
        raise_application_error(-20001, 'No hay suficiente stock para realizar la venta');
    end if;
end;
/
```


# 18. Ingrese un registro en "ventas", un día y horario permitido. Si es necesario, modifique la fecha y la hora del sistema.
```sql
insert into ventas values (160, 1, sysdate);
```


# 19. Verifique que se ha agregado un registro en "ventas" y se ha disminuido el stock en "articulos"
```sql
select * from articulos;
select * from ventas;
```


# 20. Ingrese un registro en "ventas", un día permitido fuera del horario permitido (si es necesario, modifique la fecha y hora del sistema) Se muestra un mensaje de error.
```sql
insert into ventas values (173, 2, sysdate);
```


# 21. Ingrese un registro en "ventas", un día sábado a las 15 hs.
```sql
insert into ventas values (234, 3, to_date('2023-06-17 15:00:00', 'YYYY-MM-DD HH24:MI:SS'));
```


# 22. El comercio quiere que los registros de la tabla "articulos" puedan ser ingresados, modificados y/o eliminados únicamente los sábados de 8 a 12 hs.
# Cree un trigger "tr_articulos" que NO permita que se realicen inserciones, actualizaciones ni eliminaciones en "articulos" fuera del horario especificado los días sábados, mostrando un mensaje de error.
# Recuerde que al ingresar un registro en "ventas", se actualiza el "stock" en "articulos"; el trigger debe permitir las actualizaciones del campo "stock" en "articulos" de lunes a viernes de 8 a 18 hs. (horario de ventas)
```sql
create or replace trigger tr_articulos
    before insert or update or delete on articulos
    for each row
declare
    dia_semana varchar2(20);
    hora_actual varchar2(20);
begin
    select to_char(sysdate, 'Day') into dia_semana from dual;
    select to_char(sysdate, 'HH24:MI:SS') into hora_actual from dual;

    if dia_semana = 'Saturday' and hora_actual not between '08:00:00' and '12:00:00' then
        raise_application_error(-20001, 'No se pueden realizar modificaciones en artículos fuera del horario permitido los sábados');
    end if;
end;
/
```


# 23. Ingrese un nuevo artículo un sábado a las 9 AM Note que se activan 2 triggers.
```sql
insert into articulos values (9999, 'Nuevo artículo', 9.99, 10);
```


# 24. Elimine un artículo, un sábado a las 16 hs. Mensaje de error.
```sql
delete from articulos where codigo = 9999;
```


# 25. Actualice el precio de un artículo, un domingo Mensaje de error.
```sql
update articulos set precio = 99.99 where codigo = 160;
```


# 26. Actualice el precio de un artículo, un lunes en horario de ventas Mensaje de error.
```sql
update articulos set precio = 99.99 where codigo = 160;
```


# 27. El comercio desea realizar un respaldo de la tabla "ventas" todos los días a las 23:59:59. Cree un procedimiento "respaldo_ventas" que inserte los registros de "ventas" en una tabla "respaldo_ventas".
```sql
create or replace procedure respaldo_ventas is
begin
    insert into respaldo_ventas select * from ventas;
    dbms_output.put_line('Se realizó el respaldo de ventas');
end;
/
```


# 28. Realice un respaldo manual de la tabla "ventas".
```sql
execute respaldo_ventas;
```


# 29. Muestre todos los registros de "respaldo_ventas".
```sql
select * from respaldo_ventas;
```


# 30. El comercio desea ver el total de ventas realizadas por día. Cree una función "total_ventas_dia" que tome como parámetro una fecha y devuelva el total de ventas realizadas en esa fecha.
```sql
create or replace function total_ventas_dia(fecha in date) return number is
    total number;
begin
    select sum(cantidad) into total from ventas where trunc(fecha) = trunc(fecha);
    return total;
end;
/
```


# 31. Obtenga el total de ventas realizadas el 12 de junio de 2023 utilizando la función creada.
```sql
declare
    total_ventas number;
begin
    total_ventas := total_ventas_dia(to_date('2023-06-12', 'YYYY-MM-DD'));
    dbms_output.put_line('Total de ventas realizadas el 12 de junio de 2023: ' || total_ventas);
end;
/
```