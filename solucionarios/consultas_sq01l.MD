# sesion03

## practica de consulta ejercicio01

```sql
-- Elimine la tabla "agenda"
-- Si no existe, se mostrará un mensaje indicando tal situación.

-- Comando SQL
DROP TABLE agenda;

-- Salida
Mensaje: La tabla "AGENDA" no existe.

-- Intente crear una tabla llamada "*agenda"
-- Se mostrará un mensaje de error indicando que se usó un carácter inválido ("*") para el nombre de la tabla.

-- Comando SQL
CREATE TABLE *agenda(
    apellido VARCHAR2(30),
    nombre VARCHAR2(20),
    domicilio VARCHAR2(30),
    telefono VARCHAR2(11)
);

-- Salida
Error: ORA-00911: invalid character

-- Cree una tabla llamada "agenda" con los campos especificados:
-- apellido, varchar2(30); nombre, varchar2(20); domicilio, varchar2(30); telefono, varchar2(11)
-- Se mostrará un mensaje indicando que la tabla ha sido creada exitosamente.

-- Comando SQL
CREATE TABLE agenda(
    apellido VARCHAR2(30),
    nombre VARCHAR2(20),
    domicilio VARCHAR2(30),
    telefono VARCHAR2(11)
);

-- Salida
Mensaje: La tabla "AGENDA" ha sido creada exitosamente.

-- Intente crearla nuevamente.
-- Aparecerá un mensaje de error indicando que el nombre ya está en uso por otro objeto.

-- Comando SQL
CREATE TABLE agenda(
    apellido VARCHAR2(30),
    nombre VARCHAR2(20),
    domicilio VARCHAR2(30),
    telefono VARCHAR2(11)
);

-- Salida 
Error: ORA-00955: name is already used by an existing object

-- Visualice las tablas existentes (all_tables)
-- La tabla "agenda" aparecerá en la lista.

-- Comando SQL
SELECT table_name FROM all_tables;

-- Salida
| TABLE_NAME |
|------------|
| AGENDA     |
| ...        |

-- Visualice la estructura de la tabla "agenda" (describe)
-- Aparecerá la siguiente tabla:

-- Comando SQL
DESCRIBE agenda;

-- Salida 
Name    Null     Type        
------- -------- ------------
APELLIDO          VARCHAR2(30)
NOMBRE            VARCHAR2(20)
DOMICILIO         VARCHAR2(30)
TELEFONO          VARCHAR2(11)
```






# EJERCICIO 02

```sql
-- Elimine la tabla "libros" si existe
-- Si no existe, se mostrará un mensaje indicando tal situación.

-- Comando SQL
DROP TABLE libros;

-- Salida
Mensaje: La tabla "LIBROS" no existe.

-- Verifique que la tabla "libros" no existe (all_tables)
-- No aparece en la lista.

-- Comando SQL
SELECT table_name FROM all_tables WHERE table_name = 'LIBROS';

-- Salida: No se mostrará ninguna fila

-- Cree una tabla llamada "libros" con los campos especificados:
-- titulo, varchar2(20); autor, varchar2(30); editorial, varchar2(15)
-- Se mostrará un mensaje indicando que la tabla ha sido creada exitosamente.

-- Comando SQL
CREATE TABLE libros(
    titulo VARCHAR2(20),
    autor VARCHAR2(30),
    editorial VARCHAR2(15)
);

-- Salida
Mensaje: La tabla "libros" ha sido creada exitosamente.

-- Intente crearla nuevamente.
-- Aparecerá un mensaje de error indicando que el nombre ya está en uso por otro objeto.

-- Comando SQL
CREATE TABLE libros(
    titulo VARCHAR2(20),
    autor VARCHAR2(30),
    editorial VARCHAR2(15)
);

-- Salida
Error: ORA-00955: name is already used by an existing object

-- Visualice las tablas existentes

-- Comando SQL
SELECT table_name FROM all_tables;

-- Salida
| TABLE_NAME |
|------------|
| ...        |
|  AGENDA    |
|  LIBROS    |

-- Visualice la estructura de la tabla "libros"
-- Aparecerá "libros" en la lista.

-- Comando SQL
DESCRIBE libros;

-- Salida
| Name      | Null | Type         |
|-----------|------|--------------|
| TITULO    |      | VARCHAR2(20) |
| AUTOR     |      | VARCHAR2(30) |
| EDITORIAL |      | VARCHAR2(15) |

-- Elimine la tabla "libros"

-- Comando SQL
DROP TABLE libros;

-- Salida
Mensaje: La tabla "libros" ha sido eliminada exitosamente.

-- Intente eliminar la tabla nuevamente.
-- Un mensaje indica que la tabla no existe.

-- Comando SQL
DROP TABLE libros;

-- Salida
Mensaje: La tabla "libros" no existe.
```
# PRACTICA 02
## EJERCICIO 01
```sql
-- Elimine la tabla "agenda"

-- Comando SQL
DROP TABLE agenda;

-- Salida esperada
Mensaje: La tabla "agenda" ha sido eliminada exitosamente.

-- Cree una tabla llamada "agenda" con los campos especificados:
-- apellido (cadena de 30), nombre (cadena de 20), domicilio (cadena de 30) y telefono (cadena de 11)
-- Se mostrará un mensaje indicando que la tabla ha sido creada exitosamente.

-- Comando SQL
CREATE TABLE agenda(
    apellido VARCHAR2(30),
    nombre VARCHAR2(20),
    domicilio VARCHAR2(30),
    telefono VARCHAR2(11)
);

-- Salida esperada
Mensaje: La tabla "agenda" ha sido creada exitosamente.

-- Visualice las tablas existentes para verificar la creación de "agenda" (all_tables)

-- Comando SQL
SELECT table_name FROM all_tables;

-- Salida esperada
| TABLE_NAME |
|------------|
| ...        |
| agenda     |

-- Visualice la estructura de la tabla "agenda" (describe)

-- Comando SQL
DESCRIBE agenda;

-- Salida esperada
| Name      | Null | Type         |
|-----------|------|--------------|
| APELLIDO  |      | VARCHAR2(30) |
| NOMBRE    |      | VARCHAR2(20) |
| DOMICILIO |      | VARCHAR2(30) |
| TELEFONO  |      | VARCHAR2(11) |

-- Ingrese los siguientes registros:

-- Comando SQL
INSERT INTO agenda (apellido, nombre, domicilio, telefono) VALUES ('Moreno','Alberto','Colon 123','4234567');
INSERT INTO agenda (apellido, nombre, domicilio, telefono) VALUES ('Torres','Juan','Avellaneda 135','4458787');

-- Seleccione todos los registros de la tabla.

-- Comando SQL
SELECT * FROM agenda;

-- Salida esperada
| APELLIDO | NOMBRE  | DOMICILIO       | TELEFONO  |
|----------|---------|----------------|-----------|
| Moreno   | Alberto | Colon 123      | 4234567   |
| Torres   | Juan    | Avellaneda 135 | 4458787   |

-- Elimine la tabla "agenda"

-- Comando SQL
DROP TABLE agenda;

-- Salida esperada
Mensaje: La tabla "agenda" ha sido eliminada exitosamente.

-- Intente eliminar la tabla nuevamente (aparece un mensaje de error)

-- Comando SQL
DROP TABLE agenda;

-- Salida esperada
Mensaje: La tabla "agenda" no existe.
```
### EJERCICIO 02
```sql
-- Elimine la tabla "libros" 

-- Comando SQL
DROP TABLE libros;

-- Salida 
Mensaje: La tabla "LIBROS" ha sido eliminada exitosamente.

-- Cree una tabla llamada "libros" con los campos especificados.

-- Comando SQL
CREATE TABLE libros(
    titulo VARCHAR2(20),
    autor VARCHAR2(30),
    editorial VARCHAR2(15)
);

-- Salida 
Mensaje: La tabla "LIBROS" ha sido creada exitosamente.

-- Visualice las tablas existentes.

-- Comando SQL
SELECT table_name FROM all_tables;

-- Salida
| TABLE_NAME |
|------------|
| ...        |
| LIBROS     |

-- Visualice la estructura de la tabla "libros".

-- Comando SQL
DESCRIBE libros;

-- Salida
| Name      | Null | Type         |
|-----------|------|--------------|
| TITULO    |      | VARCHAR2(20) |
| AUTOR     |      | VARCHAR2(30) |
| EDITORIAL |      | VARCHAR2(15) |

-- Muestre los campos y los tipos de datos de la tabla "libros".

-- Comando SQL
SELECT column_name, data_type FROM all_tab_columns WHERE table_name = 'LIBROS';

-- Salida
| COLUMN_NAME | DATA_TYPE    |
|-------------|--------------|
| TITULO      | VARCHAR2(20) |
| AUTOR       | VARCHAR2(30) |
| EDITORIAL   | VARCHAR2(15) |

-- Ingrese los siguientes registros.

-- Comando SQL
INSERT INTO libros (titulo, autor, editorial) VALUES ('El aleph', 'Borges', 'Planeta');
INSERT INTO libros (titulo, autor, editorial) VALUES ('Martin Fierro', 'Jose Hernandez', 'Emece');
INSERT INTO libros (titulo, autor, editorial) VALUES ('Aprenda PHP', 'Mario Molina', 'Emece');

-- Salida (en caso de éxito)
Mensaje: 3 fila(s) insertada(s) correctamente.

-- Muestre todos los registros (select) de "libros".

-- Comando SQL
SELECT * FROM libros;

-- Salida
|    TITULO    |       AUTOR        |   EDITORIAL   |
|--------------|--------------------|---------------|
|   El aleph   |      Borges        |    Planeta    |
| Martin Fierro|  Jose Hernandez    |    Emece      |
| Aprenda PHP  |   Mario Molina     |    Emece      |
```
# PRACTICA 03
## EJERCICIO 01

```sql
-- Elimine la tabla "peliculas" si ya existe.

-- Comando SQL
DROP TABLE peliculas;

-- Salida 
Mensaje: La tabla "PELICULAS" no existe.

-- Cree la tabla "peliculas" con los campos especificados.

-- Comando SQL
CREATE TABLE peliculas(
    nombre VARCHAR2(20),
    actor VARCHAR2(20),
    duracion NUMBER(3,0),
    cantidad NUMBER(1,0)
);

-- Salida 
Mensaje: La tabla "PELICULAS" ha sido creada exitosamente.

-- Vea la estructura de la tabla "peliculas".

-- Comando SQL
DESCRIBE peliculas;

-- Salida
|  Name   |  Null  |   Type    |
|---------|--------|-----------|
| NOMBRE  |        | VARCHAR2  |
| ACTOR   |        | VARCHAR2  |
| DURACION|        | NUMBER    |
| CANTIDAD|        | NUMBER    |

-- Ingrese los siguientes registros.

-- Comando SQL
INSERT INTO peliculas (nombre, actor, duracion, cantidad) VALUES ('Mision imposible', 'Tom Cruise', 128, 3);
INSERT INTO peliculas (nombre, actor, duracion, cantidad) VALUES ('Mision imposible 2', 'Tom Cruise', 130, 2);
INSERT INTO peliculas (nombre, actor, duracion, cantidad) VALUES ('Mujer bonita', 'Julia Roberts', 118, 3);
INSERT INTO peliculas (nombre, actor, duracion, cantidad) VALUES ('Elsa y Fred', 'China Zorrilla', 110, 2);

-- Salida (en caso de éxito)
Mensaje: 4 fila(s) insertada(s) correctamente.

-- Muestre todos los registros de la tabla "peliculas".

-- Comando SQL
SELECT * FROM peliculas;

-- Salida
|       NOMBRE       |      ACTOR      | DURACION | CANTIDAD |
|--------------------|-----------------|----------|----------|
| Mision imposible   |   Tom Cruise    |   128    |    3     |
| Mision imposible 2 |   Tom Cruise    |   130    |    2     |
| Mujer bonita       | Julia Roberts   |   118    |    3     |
| Elsa y Fred        | China Zorrilla  |   110    |    2     |

-- Intente ingresar una película con valor de cantidad fuera del rango permitido.

-- Comando SQL
INSERT INTO peliculas (nombre, actor, duracion, cantidad)
VALUES ('Mujer bonita', 'Richard Gere', 1200, 10);

-- Salida
Error: ORA-01438: value larger than specified precision allowed for this column

-- Ingrese un valor con decimales en un nuevo registro, en el campo "duracion".

-- Comando SQL
INSERT INTO peliculas (nombre, actor, duracion, cantidad)
VALUES ('Mujer bonita', 'Richard Gere', 120.20, 4);

-- Salida 
Mensaje: 1 fila(s) insertada(s) correctamente.

-- Muestre todos los registros de la tabla "peliculas".

-- Comando SQL
SELECT * FROM peliculas;

-- Salida
|       NOMBRE       |       ACTOR        | DURACION | CANTIDAD |
|--------------------|--------------------|----------|----------|
| Mision imposible   |    Tom Cruise      |   128    |    3     |
| Mision imposible 2 |    Tom Cruise      |   130    |    2     |
| Mujer bonita       |   Julia Roberts    |   118    |    3     |
| Elsa y Fred        |  China Zorrilla    |   110    |    2     |
| Mujer bonita       |   Richard Gere     |   120.2  |    4     |

-- Intente ingresar un nombre de película que supere los 20 caracteres.

-- Comando SQL
INSERT INTO peliculas (nombre, actor, duracion, cantidad)
VALUES ('Esta es una película con un nombre muy largo', 'Actor X', 90, 1);

-- Salida
Error: ORA-12899: value too large for column "PELICULAS"."NOMBRE" (actual: 40, maximum: 20)
```
### EJERCICIO 02
```sql
-- Elimine la tabla "empleados" si existe.

-- Comando SQL
DROP TABLE empleados;

-- Salida 
Mensaje: La tabla "EMPLEADOS" no existe.

-- Cree la tabla "empleados" con los campos especificados.

-- Comando SQL
CREATE TABLE empleados (
    nombre VARCHAR2(20),
    documento VARCHAR2(8),
    sexo VARCHAR2(1),
    domicilio VARCHAR2(30),
    sueldobasico NUMBER(6,2)
);

-- Salida 
Mensaje: La tabla "EMPLEADOS" ha sido creada exitosamente.

-- Verifique que la tabla existe consultando "all_tables"

-- Comando SQL
SELECT table_name FROM all_tables WHERE table_name = 'EMPLEADOS';

-- Salida
| TABLE_NAME |
|------------|
| EMPLEADOS  |

-- Vea la estructura de la tabla (5 campos)

-- Comando SQL
DESCRIBE empleados;

-- Salida
|  Columna   | Tipo    |
|------------|---------|
| NOMBRE     | VARCHAR2(20) |
| DOCUMENTO  | VARCHAR2(8)  |
| SEXO       | VARCHAR2(1)  |
| DOMICILIO  | VARCHAR2(30) |
| SUELDOBASICO | NUMBER(6,2) |

-- Ingrese algunos registros

-- Comando SQL
INSERT INTO empleados (nombre, documento, sexo, domicilio, sueldobasico) VALUES ('Juan Perez', '22333444', 'm', 'Sarmiento 123', 500);
INSERT INTO empleados (nombre, documento, sexo, domicilio, sueldobasico) VALUES ('Ana Acosta', '24555666', 'f', 'Colon 134', 650);
INSERT INTO empleados (nombre, documento, sexo, domicilio, sueldobasico) VALUES ('Bartolome Barrios', '27888999', 'm', 'Urquiza 479', 800);

-- Salida 
Mensaje: 3 registros insertados correctamente.

-- Seleccione todos los registros (3 registros)

-- Comando SQL
SELECT * FROM empleados;

-- Salida
|   NOMBRE       | DOCUMENTO | SEXO |   DOMICILIO     | SUELDOBASICO |
|----------------|-----------|------|-----------------|--------------|
| Juan Perez     | 22333444  | m    | Sarmiento 123   | 500.00       |
| Ana Acosta     | 24555666  | f    | Colon 134       | 650.00       |
| Bartolome Barrios | 27888999  | m    | Urquiza 479     | 800.00       |

-- Intente ingresar un registro con el valor "masculino" en el campo "sexo".
-- Un mensaje de error indica que el campo está definido para almacenar solo 1 caracter como máximo y se están ingresando 9 caracteres.

-- Comando SQL
INSERT INTO empleados (nombre, documento, sexo, domicilio, sueldobasico) VALUES ('Juanita Perez', '22333445', 'masculino', 'Sarmiento 125', 600);

-- Salida
Error: ORA-12899: valor de cadena de caracteres demasiado largo para la columna

-- Intente ingresar un valor fuera de rango, en un nuevo registro, para el campo "sueldobasico"
-- Mensaje de error

-- Comando SQL
INSERT INTO empleados (nombre, documento, sexo, domicilio, sueldobasico) VALUES ('María López', '25666777', 'f', 'San Martín 246', 10000);

-- Salida
Error: ORA-01438: valor de columna más grande que la longitud máxima especificada (6)

-- Elimine la tabla "empleados"

-- Comando SQL
DROP TABLE empleados;

-- Salida 
Mensaje: La tabla "EMPLEADOS" ha sido eliminada exitosamente.

```