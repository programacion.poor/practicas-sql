# Solucionario de Vistas (with check option)

# Ejercicios propuestos

# 1. Elimine la tabla
```sql
drop table clientes;
```


# 2. Cree la tabla
```sql
create table clientes(
    nombre varchar2(40),
    documento char(8),
    domicilio varchar2(30),
    ciudad varchar2(30)
);
```


# 3. Ingrese algunos registros
```sql
insert into clientes values('Juan Perez','22222222','Colon 1123','Cordoba');
insert into clientes values('Karina Lopez','23333333','San Martin 254','Cordoba');
insert into clientes values('Luis Garcia','24444444','Caseros 345','Cordoba');
insert into clientes values('Marcos Gonzalez','25555555','Sucre 458','Santa Fe');
insert into clientes values('Nora Torres','26666666','Bulnes 567','Santa Fe');
insert into clientes values('Oscar Luque','27777777','San Martin 786','Santa Fe');
insert into clientes values('Pedro Perez','28888888','Colon 234','Buenos Aires');
insert into clientes values('Rosa Rodriguez','29999999','Avellaneda 23','Buenos Aires');
```


# 4. Cree o reemplace la vista "vista_clientes" sin emplear "with check option"
```sql
create or replace view vista_clientes as
select nombre, ciudad
from clientes
where ciudad <> 'Cordoba';
```


# 5. Cree o reemplace la vista "vista_clientes2" empleando "with check option"
```sql
create or replace view vista_clientes2 as
select nombre, ciudad
from clientes
where ciudad <> 'Cordoba'
with check option;
```


# 6. Consulte ambas vistas
```sql
select * from vista_clientes;
select * from vista_clientes2;
```


# 7. Intente modificar la ciudad del cliente "Pedro Perez" a "Cordoba" a través de la vista restringida
```sql
-- No se permite la modificación debido a la restricción
```


# 8. Realice la misma modificación que intentó en el punto anterior a través de la vista no restringida
```sql
update vista_clientes set ciudad = 'Cordoba' where nombre = 'Pedro Perez';
```


# 9. Actualice la ciudad del cliente "Oscar Luque" a "Buenos Aires" mediante la vista restringida
```sql
update vista_clientes2 set ciudad = 'Buenos Aires' where nombre = 'Oscar Luque';
```


# 10. Verifique que "Oscar Luque" aún se incluye en la vista
```sql
select * from vista_clientes2 where nombre = 'Oscar Luque';
```


# 11. Intente ingresar un cliente de "Cordoba" en la vista restringida
```sql
-- No se permite la inserción debido a la restricción
```


# 12. Ingrese el cliente anterior a través de la vista no restringida
```sql
insert into vista_clientes values ('Cliente Nuevo', 'Cordoba');
```


# 13. Ingrese un cliente de "Salta" en la vista restringida
```sql
insert into vista_clientes2 values ('Cliente Salta', 'Salta');
```


# 14. Verifique que el nuevo registro está incluido en la vista
```sql
select * from vista_clientes2 where nombre = 'Cliente Salta';
```

