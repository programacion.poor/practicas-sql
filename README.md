01. [Crear tablas (create table - describe - all_tables - drop table)](https://gitlab.com/programacion.poor/practicas-sql/-/blob/main/solucionarios/consultas_sq01l.MD)

02. [Ingresar registros (insert into- select)](https://gitlab.com/programacion.poor/practicas-sql/-/blob/main/solucionarios/solucionario02.MD)

03. [Tipos de datos](https://gitlab.com/programacion.poor/practicas-sql/-/blob/main/solucionarios/solucionario03.MD)

04. [Recuperar algunos campos (select)](https://gitlab.com/programacion.poor/practicas-sql/-/blob/main/solucionarios/solucionario04.MD)

05. [Recuperar algunos registros (where)](https://gitlab.com/programacion.poor/practicas-sql/-/blob/main/solucionarios/solucionario05.MD)

06. [Operadores relacionales](https://gitlab.com/programacion.poor/practicas-sql/-/blob/main/solucionarios/solucionario06.MD)

07. [Borrar registros (delete)](https://gitlab.com/programacion.poor/practicas-sql/-/blob/main/solucionarios/solucionario07.MD)

08. [Actualizar registros (update)](https://gitlab.com/programacion.poor/practicas-sql/-/blob/main/solucionarios/solucionario08.MD)

09. [Comentarios](https://gitlab.com/programacion.poor/practicas-sql/-/blob/main/solucionarios/solucionario09.MD)

10. [Valores nulos (null)](https://gitlab.com/programacion.poor/practicas-sql/-/blob/main/solucionarios/solucionario10.MD)

11. [Operadores relacionales (is null)](https://gitlab.com/programacion.poor/practicas-sql/-/blob/main/solucionarios/solucionario11.MD)

12. [Clave primaria (primary key)](https://gitlab.com/programacion.poor/practicas-sql/-/blob/main/solucionarios/solucionario12.MD)

13. [Vaciar la tabla (truncate table)](https://gitlab.com/programacion.poor/practicas-sql/-/blob/main/solucionarios/solucionario13.MD)

14. [Tipos de datos alfanuméricos](https://gitlab.com/programacion.poor/practicas-sql/-/blob/main/solucionarios/solucionario14.MD)

15. [Tipos de datos numéricos](https://gitlab.com/programacion.poor/practicas-sql/-/blob/main/solucionarios/solucionario15.MD)

16. [Ingresar algunos campos](https://gitlab.com/programacion.poor/practicas-sql/-/blob/main/solucionarios/solucionario16.MD)

17. [Valores por defecto (default)](https://gitlab.com/programacion.poor/practicas-sql/-/blob/main/solucionarios/solucionario17.MD)

18. [Operadores aritméticos y de concatenación (columnas calculadas)](https://gitlab.com/programacion.poor/practicas-sql/-/blob/main/solucionarios/solucionario18.MD)

19.  [Alias (encabezados de columnas)](https://gitlab.com/programacion.poor/practicas-sql/-/blob/main/solucionarios/solucionario19.MD)

20. [Funciones string](https://gitlab.com/programacion.poor/practicas-sql/-/blob/main/solucionarios/solucionario20.MD)

21. [Funciones matemáticas](https://gitlab.com/programacion.poor/practicas-sql/-/blob/main/solucionarios/solucionario21.MD)

22. [Funciones de fechas y horas](https://gitlab.com/programacion.poor/practicas-sql/-/blob/main/solucionarios/solucionario22.MD)

23. [Ordenar registros (order by)](https://gitlab.com/programacion.poor/practicas-sql/-/blob/main/solucionarios/solucionario23.MD)

24. [Operadores lógicos (and - or - not)](https://gitlab.com/programacion.poor/practicas-sql/-/blob/main/solucionarios/solucionario24.MD)

25. [Otros operadores relacionales (between)](https://gitlab.com/programacion.poor/practicas-sql/-/blob/main/solucionarios/solucionario25.MD)

26. [Otros operadores relacionales (in)](https://gitlab.com/programacion.poor/practicas-sql/-/blob/main/solucionarios/solucionario26.MD)

27. [Búsqueda de patrones (like - not like)](https://gitlab.com/programacion.poor/practicas-sql/-/blob/main/solucionarios/solucionario27.MD)

28. [Contar registros (count)](https://gitlab.com/programacion.poor/practicas-sql/-/blob/main/solucionarios/solucionario28.MD)

29. [Funciones de grupo (count - max - min - sum - avg)](https://gitlab.com/programacion.poor/practicas-sql/-/blob/main/solucionarios/solucionario29.MD)

30. [Agrupar registros (group by)](https://gitlab.com/programacion.poor/practicas-sql/-/blob/main/solucionarios/solucionario30.MD)

31. [Seleccionar grupos (Having)](https://gitlab.com/programacion.poor/practicas-sql/-/blob/main/solucionarios/solucionario31.MD)

32. [Registros duplicados (Distinct)](https://gitlab.com/programacion.poor/practicas-sql/-/blob/main/solucionarios/solucionario32.MD)

33. [Clave primaria compuesta](https://gitlab.com/programacion.poor/practicas-sql/-/blob/main/solucionarios/solucionario33.MD)

34. [Secuencias (create sequence - currval - nextval - drop sequence)](https://gitlab.com/programacion.poor/practicas-sql/-/blob/main/solucionarios/solucionario34.MD)

35. [Alterar secuencia (alter sequence)](https://gitlab.com/programacion.poor/practicas-sql/-/blob/main/solucionarios/solucionario35.MD)

36. [Integridad de datos](https://gitlab.com/programacion.poor/practicas-sql/-/blob/main/solucionarios/solucionario36.MD)

37. [Restricción primary key](https://gitlab.com/programacion.poor/practicas-sql/-/blob/main/solucionarios/solucionario37.MD)

38. [Restricción unique](https://gitlab.com/programacion.poor/practicas-sql/-/blob/main/solucionarios/solucionario38.MD)

39. [Restriccioncheck](https://gitlab.com/programacion.poor/practicas-sql/-/blob/main/solucionarios/solucionario39.MD)

40. [Restricciones: validación y estados (validate - novalidate - enable - disable)](https://gitlab.com/programacion.poor/practicas-sql/-/blob/main/solucionarios/solucionario40.MD)

41. [Restricciones: información (user_constraints - user_cons_columns)](https://gitlab.com/programacion.poor/practicas-sql/-/blob/main/solucionarios/solucionario41.MD)

42. [Restricciones: eliminación (alter table - drop constraint)](https://gitlab.com/programacion.poor/practicas-sql/-/blob/main/solucionarios/solucionario42.MD)

43. [Indices](https://gitlab.com/programacion.poor/practicas-sql/-/blob/main/solucionarios/solucionario43.MD)

44. [Indices (Crear . Información)](https://gitlab.com/programacion.poor/practicas-sql/-/blob/main/solucionarios/solucionario44.MD)

45. [Indices (eliminar)](https://gitlab.com/programacion.poor/practicas-sql/-/blob/main/solucionarios/solucionario45.MD)

46. [Varias tablas (join)](https://gitlab.com/programacion.poor/practicas-sql/-/blob/main/solucionarios/solucionario46.MD)

47. [Combinación interna (join)](https://gitlab.com/programacion.poor/practicas-sql/-/blob/main/solucionarios/solucionario47.MD)

48. [Combinación externa izquierda (left join)](https://gitlab.com/programacion.poor/practicas-sql/-/blob/main/solucionarios/solucionario48.MD)

49. [Combinación externa derecha (right join)](https://gitlab.com/programacion.poor/practicas-sql/-/blob/main/solucionarios/solucionario49.MD)

50. [Combinación externa completa (full join)](https://gitlab.com/programacion.poor/practicas-sql/-/blob/main/solucionarios/solucionario50.MD)

51. [Combinaciones cruzadas (cross)](https://gitlab.com/programacion.poor/practicas-sql/-/blob/main/solucionarios/solucionario51.MD)

52. [Autocombinación](https://gitlab.com/programacion.poor/practicas-sql/-/blob/main/solucionarios/solucionario52.MD)

53. [Combinaciones y funciones de agrupamiento](https://gitlab.com/programacion.poor/practicas-sql/-/blob/main/solucionarios/solucionario53.MD))

54. [Combinar más de 2 tablas](https://gitlab.com/programacion.poor/practicas-sql/-/blob/main/solucionarios/solucionario54.MD)

55. [Otros tipos de combinaciones](https://gitlab.com/programacion.poor/practicas-sql/-/blob/main/solucionarios/solucionario55.MD)

56. [Clave foránea](https://gitlab.com/programacion.poor/practicas-sql/-/blob/main/solucionarios/solucionario56.MD)

57. [Restricciones (foreign key)](https://gitlab.com/programacion.poor/practicas-sql/-/blob/main/solucionarios/solucionario57.MD)

58. [Restricciones foreign key en la misma tabla](https://gitlab.com/programacion.poor/practicas-sql/-/blob/main/solucionarios/solucionario58.MD)

59. [Restricciones foreign key (eliminación)](https://gitlab.com/programacion.poor/practicas-sql/-/blob/main/solucionarios/solucionario59.MD)

60. [Restricciones foreign key deshabilitar y validar](https://gitlab.com/programacion.poor/practicas-sql/-/blob/main/solucionarios/solucionario60.MD)

61. [Restricciones foreign key (acciones)](https://gitlab.com/programacion.poor/practicas-sql/-/blob/main/solucionarios/solucionario61.MD)

62. [Información de user_constraints](https://gitlab.com/programacion.poor/practicas-sql/-/blob/main/solucionarios/solucionario62.MD)

63. [Restricciones al crear la tabla](https://gitlab.com/programacion.poor/practicas-sql/-/blob/main/solucionarios/solucionario63.MD)

64. [Unión](https://gitlab.com/programacion.poor/practicas-sql/-/blob/main/solucionarios/solucionario64.MD)

65. [Intersección](https://gitlab.com/programacion.poor/practicas-sql/-/blob/main/solucionarios/solucionario65.MD)

66. [Minus](https://gitlab.com/programacion.poor/practicas-sql/-/blob/main/solucionarios/solucionario66.MD)

67. [Agregar campos (alter table-add)](https://gitlab.com/programacion.poor/practicas-sql/-/blob/main/solucionarios/solucionario67.MD)

68. [Modificar campos (alter table - modify)](https://gitlab.com/programacion.poor/practicas-sql/-/blob/main/solucionarios/solucionario68.MD)

69. [Eliminar campos (alter table - drop)](https://gitlab.com/programacion.poor/practicas-sql/-/blob/main/solucionarios/solucionario69.MD)

70.  [Agregar campos y restricciones (alter table)](https://gitlab.com/programacion.poor/practicas-sql/-/blob/main/solucionarios/solucionario70.MD)

71. [Subconsultas](https://gitlab.com/programacion.poor/practicas-sql/-/blob/main/solucionarios/solucionario71.MD)

72. [Subconsultas como expresion](https://gitlab.com/programacion.poor/practicas-sql/-/blob/main/solucionarios/solucionario72.MD)

73. [Subconsultas con in](https://gitlab.com/programacion.poor/practicas-sql/-/blob/main/solucionarios/solucionario73.MD)

74. [Subconsultas any- some - all](https://gitlab.com/programacion.poor/practicas-sql/-/blob/main/solucionarios/solucionario74.MD)

75. [Subconsultas correlacionadas](https://gitlab.com/programacion.poor/practicas-sql/-/blob/main/solucionarios/solucionario75.MD)

76. [Exists y No Exists](https://gitlab.com/programacion.poor/practicas-sql/-/blob/main/solucionarios/solucionario76.MD)

77. [Subconsulta simil autocombinacion](https://gitlab.com/programacion.poor/practicas-sql/-/blob/main/solucionarios/solucionario77.MD)

78. [Subconsulta conupdate y delete](https://gitlab.com/programacion.poor/practicas-sql/-/blob/main/solucionarios/solucionario78.MD)

79. [Subconsulta e insert](https://gitlab.com/programacion.poor/practicas-sql/-/blob/main/solucionarios/solucionario79.MD)

80. [Crear tabla a partir de otra (create table-select)](https://gitlab.com/programacion.poor/practicas-sql/-/blob/main/solucionarios/solucionario80.MD)

81. [Vistas (create view)](https://gitlab.com/programacion.poor/practicas-sql/-/blob/main/solucionarios/solucionario81.MD)

82. [Vistas (información)](https://gitlab.com/programacion.poor/practicas-sql/-/blob/main/solucionarios/solucionario82.MD)

83. [Vistas eliminar (drop view)](https://gitlab.com/programacion.poor/practicas-sql/-/blob/main/solucionarios/solucionario83.MD)

84. [Vistas (modificar datos a través de ella)](https://gitlab.com/programacion.poor/practicas-sql/-/blob/main/solucionarios/solucionario84.MD)

85. [Vistas (with read only)](https://gitlab.com/programacion.poor/practicas-sql/-/blob/main/solucionarios/solucionario85.MD)

86. [Vistas modificar (create or replace view)](https://gitlab.com/programacion.poor/practicas-sql/-/blob/main/solucionarios/solucionario86.MD)

87. [Vistas (with check option)](https://gitlab.com/programacion.poor/practicas-sql/-/blob/main/solucionarios/solucionario87.MD)

88. [Vistas (otras consideraciones: force)](https://gitlab.com/programacion.poor/practicas-sql/-/blob/main/solucionarios/solucionario88.MD)

89. [Vistas materializadas (materialized view)](https://gitlab.com/programacion.poor/practicas-sql/-/blob/main/solucionarios/solucionario89.MD)

90. [Procedimientos almacenados](https://gitlab.com/programacion.poor/practicas-sql/-/blob/main/solucionarios/solucionario90.MD)

91. [Procedimientos Almacenados (crear- ejecutar)](https://gitlab.com/programacion.poor/practicas-sql/-/blob/main/solucionarios/solucionario91.MD)

92. [Procedimientos Almacenados (eliminar)](https://gitlab.com/programacion.poor/practicas-sql/-/blob/main/solucionarios/solucionario92.MD)

93. [Procedimientos almacenados (parámetros de entrada)](https://gitlab.com/programacion.poor/practicas-sql/-/blob/main/solucionarios/solucionario93.MD)

94. [Procedimientos almacenados (variables)](https://gitlab.com/programacion.poor/practicas-sql/-/blob/main/solucionarios/solucionario94.MD)

95. [Procedimientos Almacenados (informacion)](https://gitlab.com/programacion.poor/practicas-sql/-/blob/main/solucionarios/solucionario95.MD)

96. [Funciones](https://gitlab.com/programacion.poor/practicas-sql/-/blob/main/solucionarios/solucionario96.MD)

97. [Control de flujo (if)](https://gitlab.com/programacion.poor/practicas-sql/-/blob/main/solucionarios/solucionario97.MD)

98. [Control de flujo (case)](https://gitlab.com/programacion.poor/practicas-sql/-/blob/main/solucionarios/solucionario98.MD)

99. [Control de flujo (loop)](https://gitlab.com/programacion.poor/practicas-sql/-/blob/main/solucionarios/solucionario99.MD)

100. [Control de flujo (for)](https://gitlab.com/programacion.poor/practicas-sql/-/blob/main/solucionarios/solucionario100.MD)

101. [Control de flujo (while loop)](https://gitlab.com/programacion.poor/practicas-sql/-/blob/main/solucionarios/solucionario101.MD)

102. [Disparador (trigger)](https://gitlab.com/programacion.poor/practicas-sql/-/blob/main/solucionarios/solucionario102.MD)

103. [Disparador (información)](https://gitlab.com/programacion.poor/practicas-sql/-/blob/main/solucionarios/solucionario103.MD)

104. [Disparador de inserción a nivel de sentencia](https://gitlab.com/programacion.poor/practicas-sql/-/blob/main/solucionarios/solucionario104.MD)

105. [Disparador de insercion a nivel de fila (insert trigger for each row)](https://gitlab.com/programacion.poor/practicas-sql/-/blob/main/solucionarios/solucionario105.MD)

106. [Disparador de borrado (nivel de sentencia y de fila)](https://gitlab.com/programacion.poor/practicas-sql/-/blob/main/solucionarios/solucionario106.MD)

107. [Disparador de actualizacion a nivel de sentencia (update trigger)](https://gitlab.com/programacion.poor/practicas-sql/-/blob/main/solucionarios/solucionario107.MD)

108. [Disparador de actualización a nivel de fila (update trigger)](https://gitlab.com/programacion.poor/practicas-sql/-/blob/main/solucionarios/solucionario108.MD)

109. [Disparador de actualización - lista de campos (update trigger)](https://gitlab.com/programacion.poor/practicas-sql/-/blob/main/solucionarios/solucionario109.MD)

110. [Disparador de múltiples eventos](https://gitlab.com/programacion.poor/practicas-sql/-/blob/main/solucionarios/solucionario110.MD)

111. [Disparador (old y new)](https://gitlab.com/programacion.poor/practicas-sql/-/blob/main/solucionarios/solucionario111.MD)

112. [Disparador condiciones (when)](https://gitlab.com/programacion.poor/practicas-sql/-/blob/main/solucionarios/solucionario112.MD)

113. [Disparador de actualizacion - campos (updating)](https://gitlab.com/programacion.poor/practicas-sql/-/blob/main/solucionarios/solucionario113.MD)

114. [Disparadores (habilitar y deshabilitar)](https://gitlab.com/programacion.poor/practicas-sql/-/blob/main/solucionarios/solucionario114.MD)

115. [Disparador (eliminar)](https://gitlab.com/programacion.poor/practicas-sql/-/blob/main/solucionarios/solucionario115.MD)

116. [Errores definidos por el usuario en trigger](https://gitlab.com/programacion.poor/practicas-sql/-/blob/main/solucionarios/solucionario116.MD)

117. [Seguridad y acceso a Oracle](https://gitlab.com/programacion.poor/practicas-sql/-/blob/main/solucionarios/solucionario117.MD)

118. [Usuarios (crear)](https://gitlab.com/programacion.poor/practicas-sql/-/blob/main/solucionarios/solucionario118.MD)

119. [Permiso de conexión](https://gitlab.com/programacion.poor/practicas-sql/-/blob/main/solucionarios/solucionario119.MD)

120. [Privilegios del sistema (conceder)](https://gitlab.com/programacion.poor/practicas-sql/-/blob/main/solucionarios/solucionario120.MD)

121. [Privilegios del sistema (with admin option)](https://gitlab.com/programacion.poor/practicas-sql/-/blob/main/solucionarios/solucionario121.MD)

122. [Modelado de base de datos](https://gitlab.com/programacion.poor/practicas-sql/-/blob/main/practicahabilidades.pdf)
